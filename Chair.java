
public class Chair extends Furniture{
	private String color;
	
	public Chair(String model, int weight, int category,String color) {
		super(model, weight, category);
		this.color=color;
		
	}
	
	public String getColor() {
		return color;
	}

	public double calculatePrice() {
		double currentPrice = 100 ;
		
		 if(this.getCategory()==3) {
			 currentPrice=100+(0.3*currentPrice);
		 }
		 return currentPrice;
	}
	

	public String chooseCategory() {
		String aCategory="";
		 if(this.getCategory()==1) {
			 return aCategory ="Office chair";
		 }
		 else if(this.getCategory()==2) {
			 return aCategory = "Cooperation chair";
		 }else if(this.getCategory()==3) {
			 aCategory = "Armchair";
		 }
		 return aCategory;
	}
	
	public String getInfo() {
		return super.getInfo() + "Fabric color: "+this.getColor();
	}
}
