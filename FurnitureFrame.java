import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FurnitureFrame extends JFrame{
	
	private JPanel panel = new JPanel();
	private JButton b1 = new JButton("Search Furniture");
	private JButton b2 = new JButton("Print Furniture to Text File");
	private JTextField t1 = new JTextField("Enter model");
	private JTextField t2 = new JTextField("Enter Category");
	
	ArrayList<Furniture> allFurniture = new ArrayList<Furniture>();

	public FurnitureFrame(ArrayList<Furniture> furnitures) {
		
		allFurniture=furnitures;
		this.setVisible(true);
		this.setTitle("Furniture Company");
		this.setSize(300,250);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel.add(t1);
		panel.add(b1);
		panel.add(t2);
		panel.add(b2);
		
		this.setContentPane(panel);
		
		Collections.sort(furnitures);
		
		b1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				String modelName =t1.getText();
				boolean found = false;
				for(Furniture furniture: allFurniture) {
					if(furniture.getModel().equals(modelName)) {
					     found = true;
					     System.out.println(furniture.getInfo());
					     break;
					}
				}
				if(!found) {
					System.out.println("There is no furniture of model "+modelName);
				}
			}
		});
		
		b2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				String textCategory=t2.getText();
				File f = new File(textCategory+".txt");
				try {
					FileWriter write = new FileWriter(f);
					for(Furniture furniture:furnitures) {
						if(furniture.chooseCategory().equals(textCategory)) {
							write.write(furniture.getInfo());
							for(int i=0; i<2; i++) write.write(System.lineSeparator());
						}
					}
					write.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
		
	}

}

