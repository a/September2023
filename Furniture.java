import java.util.ArrayList;

public abstract class Furniture implements Comparable{
	
	private String model;
	private int weight;
	private int category;
	
	private ArrayList<Furniture> allFurniture = new ArrayList<Furniture>();
	
	
	public Furniture(String model, int weight, int category) {
		
		this.model = model;
		this.weight = weight;
		this.category = category;
	}
	
	public int getCategory() {
		return category;
	}
	
	public boolean equals(Furniture o) {
		Furniture otherFurniture =(Furniture)o;
		return this.equals(otherFurniture);
		
	}

	public abstract double calculatePrice();
	public abstract String chooseCategory();
	

	public int getWeight() {
		return weight;
	}
	
	public String getModel() {
		return model;
	}
	public String getInfo() {
		return "- "+this.getClass().getName()+" info:\n"+
				this.getModel()+", "+this.calculatePrice()+" Euro: \n"+
				"Category: "+this.chooseCategory()+"\n"+
				"Weight: "+this.getWeight()+"\n";
				
	}
	@Override
	public int compareTo(Object o) {
		Furniture otherFurniture = (Furniture)o;
		return Double.compare(otherFurniture.calculatePrice(), this.calculatePrice());
	}
	
	
	
	

}
