
public class Bookcase extends Furniture{
	private int shelves;

	public Bookcase(String model, int weight, int category,int shelves) {
		super(model, weight, category);
		this.shelves=shelves;
		
	}
	
	public double calculatePrice() {
		double currentPrice = 50*shelves ;
		 if(this.getCategory()==1) {
			 currentPrice=(50*shelves)+(0.2*currentPrice);
		 }
		 else if(this.getCategory()==2) {
			 currentPrice=(50*shelves)+(0.3*currentPrice);
		 }else if(this.getCategory()==3) {
			 currentPrice=(50*shelves)+(0.5*currentPrice);
		 }
		 return currentPrice;
	}
	
	public String chooseCategory() {
		String aCategory="";
		 if(this.getCategory()==1) {
			 aCategory ="Plastic";
		 }
		 else if(this.getCategory()==2) {
			 aCategory = "Metallic";
		 }else if(this.getCategory()==3) {
			 aCategory = "Wooden";
		 }
		 return aCategory;
	}
	
	public int getShelves() {
		return shelves;
	}
	
	public String getInfo() {
		return super.getInfo() + "Shelves: "+this.getShelves();
	}

	
}
