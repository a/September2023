import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		Furniture b1 = new Bookcase("Ikea",70,3,8);
		Furniture c1 = new Chair("HermanMiller",40,2,"Grey");
		FurnitureCompany bestFurniture = new FurnitureCompany(b1,c1);
		
		bestFurniture.addFurniture(b1);
		bestFurniture.addFurniture(c1);
		
		new FurnitureFrame(bestFurniture.getAllFurnitures());
		
	}

}
