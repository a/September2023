import java.util.ArrayList;

public class FurnitureCompany{
	
	private ArrayList<Furniture> allFurniture = new ArrayList<Furniture>();

	Furniture Bookcase;
	Furniture Chair;
	
	public FurnitureCompany(Furniture Bookcase,Furniture Chair) {
		this.Bookcase=Bookcase;
		this.Chair=Chair;
		
	}

	public ArrayList<Furniture> getAllFurnitures() {
		return this.allFurniture;
	}
	
	public void addFurniture(Furniture furniture) {
		allFurniture.add(furniture);
	}
	
}
